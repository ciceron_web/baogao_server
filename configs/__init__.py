# -*- coding: utf-8 -*-
from datetime import timedelta

VERSION='0.1'
DEBUG=True

# Define the application directory
import os

#: Define the database
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
# DATABASE_CONNECT_OPTIONS = {}


#: HOST
if os.environ.get('PURPOSE') == 'PROD':
    HOST='http://baogao.co'
    #SESSION_COOKIE_DOMAIN=".ciceron.me"
    #SESSION_COOKIE_PATH="/"
elif os.environ.get('PURPOSE') == 'DEV':
    HOST='http://test.baogao.co'
    #SESSION_COOKIE_DOMAIN=".ciceron.xyz"
    #SESSION_COOKIE_PATH="/"
else:
    HOST='http://localhost'

#: Session
SESSION_TYPE='redis'
SESSION_COOKIE_NAME="CiceronCookie"
PERMANENT_SESSION_LIFETIME=timedelta(days=15)
SECRET_KEY='Yh1onQnWOJuc3OBQHhLFf5dZgogGlAnEJ83FacFv'

# SNS Login
FACEBOOK_APP_ID = "1830327450614969"
FACEBOOK_APP_SECRET = "7c1a7dd0f73ce5e1e11a805630da8830"
NAVER_CLIENT_ID = "ELlFYMSOktl1LTLYPHEC"
NAVER_CLIENT_SECRET = "PGytqBd11y"
NAVER_CALLBACK = "{}:5000/api2/v2/naverCallback".format(HOST)

#: Swagger
SWAGGER = {
    'title': 'Baogao API',
    'uiversion': 2
}


#: JSON으로 들어온 데이터들을 정렬해준다
JSON_SORT_KEYS=False

