import sys
import traceback
from flask import g, session
from app.common import lib as commonLib


def signUp(**kwargs):
    if 'referred_by' in kwargs:
        referred_by_user_email = kwargs.get('referred_by')
        referred_by_user_id = commonLib.get_user_id(g.db, referred_by_user_email)
        kwargs.pop('referred_by')
        kwargs['referred_by_user_id'] = referred_by_user_id

    is_ok = commonLib._insert(g.db, 'users', **kwargs)
    if is_ok == True:
        g.db.commit()
        return True
    else:
        g.db.rollback()
        return False

def getUserInfo(email):
    cursor = g.db.cursor()
    user_id = commonLib.get_user_id(g.db, email)
    query_userInfo = """
        SELECT
            school_id
          , enter_year
          , name
          , major
          , phone
          , wechat_id
        FROM users
        WHERE id = %s
    """
    cursor.execute(query_userInfo, (user_id, ))
    ret = cursor.fetchone()
    school_id = ret['school_id']
    enter_year = ret['enter_year']
    name = ret['name']
    major = ret['major']
    phone = ret['phone']
    wechat_id = ret['wechat_id']

    query_baogaoCnt = """
        SELECT count(*) as cnt
        FROM request
        WHERE email = %s
    """
    cursor.execute(query_baogaoCnt, (email, ))
    baogaoCnt = cursor.fetchone()['cnt']

    query_commentCnt = """
        SELECT count(*) as cnt
        FROM articles
        WHERE user_id = %s
    """
    cursor.execute(query_commentCnt, (user_id, ))
    commentCnt = cursor.fetchone()['cnt']

    query_schoolAuth = """
        SELECT count(*) as cnt
        FROM auth
        WHERE user_email = %s
          AND isAuthenticated = true
    """
    cursor.execute(query_schoolAuth, (email, ))
    cnt_auth = cursor.fetchone()['cnt']
    if cnt_auth > 0:
        is_authenticated = True
    else:
        is_authenticated = False

    query_schoolInfo = """
        SELECT
            name
          , english_name
          , url
          , email
          , color
        FROM school
        WHERE id = %s
    """
    cursor.execute(query_schoolInfo, (school_id, ))
    school_info = cursor.fetchone()
    school_name = school_info['name']
    school_engName = school_info['english_name']
    school_url = school_info['url']
    school_color = school_info['color']
    school_email_domain = school_info['email']

    return {
              'name': name
            , 'email': email
            , 'major': major
            , 'phone': phone
            , 'wechat_id': wechat_id
            , 'school_id': school_id
            , 'enter_year': enter_year
            , 'baogaoCnt': baogaoCnt
            , 'commentCnt': commentCnt
            , 'is_authenticated': is_authenticated
            , 'school_name': school_name
            , 'school_engName': school_engName
            , 'school_url': school_url
            , 'school_color': school_color
            , 'school_domain': school_email_domain
           }

def login(email, password):
    cursor = g.db.cursor()
    query = """
        SELECT count(*) as cnt
        FROM users
        WHERE email = %s AND password = %s
        """
    cursor.execute(query, (email, password, ))
    ret = cursor.fetchone()
    if ret['cnt'] != 1:
        return False, None

    data = getUserInfo(email)

    return True, data

def idUniqueCheck(email):
    cursor = g.db.cursor()
    query = """
        SELECT count(*) as cnt
        FROM users
        WHERE email = %s
    """
    cursor.execute(query, (email, ))
    check_data = cursor.fetchone()
    if check_data['cnt'] == 0:
        return True
    else:
        return False

def passwordChange(email, old_password, new_password):
    cursor = g.db.cursor()
    query_checkOldPassword = """
        SELECT count(*) as cnt
        FROM users
        WHERE email = %s AND password = %s
    """
    cursor.execute(query_checkOldPassword, (email, old_password, ))
    ret = cursor.fetchone()
    if ret['cnt'] < 1:
        return False

    query_updatePassword = """
        UPDATE users
        SET password = %s
        WHERE email = %s
    """
    try:
        cursor.execute(query_updatePassword, (new_password, email, ))
        g.db.commit()

    except:
        traceback.print_exc()
        g.db.rollback()
        return False

    return True

def passwordResetMailSend(email):
    cursor = g.db.cursor()
    user_id = commonLib.get_user_id(g.db, email)
    if user_id == -1:
        return False

    f = open('app/userControl/mail_templates/passwordRecovery.html', 'r')
    mail_mainText = f.read()
    f.close()

    temp_password = commonLib.random_string_gen(size=16)

    mail_mainText = mail_mainText.format(password=temp_password)
    is_ok = commonLib.sendMail(
                email
              , "임시 비밀번호가 발급되었습니다!"
              , mail_mainText
              )

    if is_ok == False:
        return False

    hashed_tempPassword = commonLib.get_hashed_password(temp_password)

    query = """
        UPDATE users
        SET password = %s
        WHERE email = %s
    """
    try:
        cursor.execute(query, (hashed_tempPassword, email, ))
        g.db.commit()

    except:
        traceback.print_exc()
        g.db.rollback()
        return False

    return True

