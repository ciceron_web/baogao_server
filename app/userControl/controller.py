from flask import request, json, make_response, session, g, redirect, url_for, render_template, abort
import json as nativeJson
import sys
from app import app
from app.common import lib as commonLib
from app.common.lib import login_required
from . import model
from flask_oauthlib.client import OAuth
from app.common.naver_login import flask_naver


oauth = OAuth()
facebook = oauth.remote_app('facebook',
                            base_url='https://graph.facebook.com/',
                            request_token_url=None,
                            access_token_url='/oauth/access_token',
                            authorize_url='https://www.facebook.com/dialog/oauth',
                            consumer_key=app.config['FACEBOOK_APP_ID'],
                            consumer_secret=app.config['FACEBOOK_APP_SECRET'],
                            request_token_params={'scope': ['email']}
                            )

naver = flask_naver(app)


def signUp():
    """
    회원 가입
    ---
    tags:
      - userControl
    # consumes:
    #   - application/x-www-form-urlencoded
    # produces:
    #   - application/x-www-form-urlencoded
    parameters:
      - name: email
        in: formData
        description: 이메일
        required: true
        type: string
      - name: password
        in: formData
        description: 비밀번호, SHA256 해싱해서 줄 것
        required: true
        type: string
      - name: school_id
        in: formData
        description: 학교 id
        required: true
        type: integer
      - name: major
        in: formData
        description: 전공
        required: true
        type: string
      - name: name
        in: formData
        description: 이름
        required: true
        type: string
      - name: enter_year
        in: formData
        description: 입학년도
        required: true
        type: integer
      - name: phone
        in: formData
        description: 폰번호
        required: true
        type: string
      - name: wechat_id
        in: formData
        description: 위챗 ID
        required: true
        type: string
      - name: referred_by
        in: formData
        description: 추천인 이메일
        required: true
        type: string
      - name: birth
        in: formData
        description: 생년월일
        required: true
        type: string
    responses:
      200:
        description: 성공
      410:
        description: 실패
    """
    params = request.form.to_dict()

    is_ok = model.signUp(**params)
    if is_ok == True:
        return make_response("OK", 200)
    else:
        return make_response("Fail", 410)

def sessionStatus():
    """
    세션 파라미터 표시
    ---
    tags:
        - userControl
    responses:
      410:
        description: Fail
      200:
        description: OK
        schema:
          id: session
          type: object
        examples:
          logged_id: True
          email: 이메일
          enter_year: 입학년도
          school_id: 학교ID
          name: 이름
          baogao_usage_cnt: 바오가오 사용량
          comment_cnt: 강의평 작성 사용량
    """
    return make_response(json.jsonify(**session), 200)

def login():
    """
    로그인
    ---
    tags:
      - userControl
    parameters:
      - name : email
        in : formData
        description : 이메일
        required: true
        type : string
      - name : password
        in : formData
        description : 비밀번호, SHA256 해싱해서 줄 것
        required: true
        type : string
    responses:
      200:
        description : OK
        schema:
          id: session
          type: object
        examples:
          logged_id: True
          email: 이메일
          enter_year: 입학년도
          school_id: 학교ID
          name: 이름
          baogao_usage_cnt: 바오가오 사용량
          comment_cnt: 강의평 작성 사용량
      410:
        description : Fail
    """

    email = request.form['email']
    password = request.form['password']

    is_ok, data = model.login(email, password)
    if is_ok == True:
        session['logged_in'] = True
        session['email'] = email
        for key, value in data.items():
            session[key] = value

        return make_response(json.jsonify(**session), 200)

    else:
        return make_response("Fail", 410)

def logout():
    """
    로그아웃
    ---
    tags:
      - userControl
    responses:
      410:
        description: Fail
      200:
        description: OK
    """
    keys = list(session.keys())
    for key in keys:
        session.pop(key)

    return redirect('/evaluation2/login')

def idUniqueCheck():
    """
    ID 중복체크
    ---
    tags:
      - userControl
    parameters:
      - name : email
        in : query
        description : 이메일
        required: true
        type : string
    responses:
      200:
        description : OK. 중복없음
      410:
        description : Fail. 누군가 사용중
    """
    email = request.args['email']
    is_unique = model.idUniqueCheck(email)

    if is_unique == True:
        return make_response("OK", 200)

    else:
        return make_response("Dup", 410)

@login_required
def passwordChange():
    """
    비밀번호 변경 (login_required)
    ---
    tags:
      - userControl
    parameters:
      - name : email
        in : formData
        description : 이메일
        required: true
        type : string
      - name : old_password
        in : formData
        description : 이전 비밀번호, SHA256 처리 요망
        required: true
        type : string
      - name : new_password
        in : formData
        description : 새로운 비밀번호, SHA256 처리 요망
        required: true
        type : string
    responses:
      200:
        description : OK
      410:
        description : Fail
    """
    email = request.form['email']
    old_password = request.form['old_password']
    new_password = request.form['new_password']

    is_ok = model.passwordChange(email, old_password, new_password)
    if is_ok == True:
        return make_response("OK", 200)

    else:
        return make_response("Fail", 410)

def passwordResetMailSend():
    """
    비밀번호 찾기 메일 전송
    ---
    tags:
      - userControl
    parameters:
      - name : email
        in : formData
        description : 이메일
        required: true
        type : string
    responses:
      200:
        description : OK
      410:
        description : Fail
    """
    email = request.form['email']

    is_sent = model.passwordResetMailSend(email)
    if is_sent == True: 
        return make_response("OK", 200)

    else:
        return make_response("Fail", 410)

def fbLogin():
    return facebook.authorize(callback=url_for('fbCallback', _external=True))

def fbCallback():
    resp = facebook.authorized_response()
    if resp is None or 'access_token' not in resp:
        return make_response(json.jsonify(
            message="No access token from facebook"), 403)

    session['facebook_token'] = (resp['access_token'], '')
    user_data = facebook.get('/me?fields=email').data

    email = user_data.get('email')
    if email is None:
        return "Facebook currently works abnormally. Please wait a few hours."

    user_id = commonLib.get_user_id(g.db, email)
    if user_id == -1:
        return redirect('/evaluation2/joinus?email={}'.format(email))

    else:
        data = model.getUserInfo(email)
        session['logged_in'] = True
        session['email'] = email
        for key, value in data.items():
            session[key] = value
            
        return redirect('/evaluation2')

@facebook.tokengetter
def fbGettoken(token=None):
    return session.get('facebook_token')

def naverLogin():
    return redirect(naver.login())

def naverCallback():
    if(request.args is not None):
        auth = naver.getAuth(request.args)
        if(type(auth) == dict):
            session['auth_token'] = auth.get('access_token')
            session['refresh_token'] = auth.get('refresh_token')
            session['token_type'] = auth.get('token_type')

            userinfo = nativeJson.dumps(naver.getUserInfo(session.get('token_type'), session.get('auth_token')))
            email = userinfo['email']
            user_id = commonLib.get_user_id(g.db, email)

            if user_id == -1:
                return reirect('/evaluation2/joinus?email={}'.format(email))

            else:
                is_ok, data = model.login(email, password)
                session['logged_in'] = True
                session['email'] = email
                for key, value in data.items():
                    session[key] = value

                return redirect('/evaluation2')

        else:
            return auth

def tempLogin():
    return render_template('login.html')

def tempSignUp():
    return render_template('join_us.html')

def tempFindPass():
    return render_template('find_login_info.html')
