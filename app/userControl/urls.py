from app import app, versions
import app.userControl.controller as userControl

for ver in versions:
    app.add_url_rule('{}'.format(ver), view_func=userControl.sessionStatus, methods=['GET'])
    app.add_url_rule('{}/logout'.format(ver), view_func=userControl.logout, methods=['GET'])
    app.add_url_rule('{}/signUp'.format(ver), view_func=userControl.signUp, methods=['POST'])
    app.add_url_rule('{}/login'.format(ver), view_func=userControl.login, methods=['POST'])
    app.add_url_rule('{}/idUniqueCheck'.format(ver), view_func=userControl.idUniqueCheck, methods=['GET'])
    app.add_url_rule('{}/changePassword'.format(ver), view_func=userControl.passwordChange, methods=['POST'])
    app.add_url_rule('{}/sendPasswordResetMail'.format(ver), view_func=userControl.passwordResetMailSend, methods=['POST'])
    app.add_url_rule('{}/fbLogin'.format(ver), view_func=userControl.fbLogin, methods=['GET'])
    app.add_url_rule('{}/fbCallback'.format(ver), view_func=userControl.fbCallback, methods=['GET'])
    app.add_url_rule('{}/fbGettoken'.format(ver), view_func=userControl.fbGettoken, methods=['GET'])
    app.add_url_rule('{}/naverLogin'.format(ver), view_func=userControl.naverLogin, methods=['GET'])
    app.add_url_rule('{}/naverCallback'.format(ver), view_func=userControl.naverCallback, methods=['GET'])

    app.add_url_rule('/evaluation2/login'.format(ver), view_func=userControl.tempLogin, methods=['GET'])
    app.add_url_rule('/evaluation2/joinus'.format(ver), view_func=userControl.tempSignUp, methods=['GET'])
    app.add_url_rule('/evaluation2/find_login_info'.format(ver), view_func=userControl.tempFindPass, methods=['GET'])

