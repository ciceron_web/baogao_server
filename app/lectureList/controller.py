from flask import request, json, make_response
from . import model
import sys
from app.common import lib as commonLib
from app.common.lib import login_required
from app.common.lib import cralwer_blocker


@cralwer_blocker
def newLectureComments():
    """
    새 강의평들
    ---
    tags:
      - lectureList
    parameters:
      - name : page
        in : query
        description: 페이징 기본값 1
        required : true
        type : integer
    responses:
      200:
        description :
          - 성공, 1 page 당 아이템 12개
        examples:
          “data”:[/* items */]
      400:
        description : 실패
    """
    page = int(request.args.get('page', 1))
    data = model.newLectureComments(page=page)
    return make_response(json.jsonify(data=data), 200)

@cralwer_blocker
def recommendLectureComments():
    """
    추천강의
    ---
    tags:
      - lectureList
    parameters:
      - name : page
        in : query
        description: 페이징
        required : true
        type : integer
    responses:
      200:
        description : 성공, 1 page 당 아이템 12개
        # examples:
        #   data:[/* items */]
        #   item 단위:
        #   school_id: Integer, 학교 ID
        #   school_name: String, 학교 이름
        #   lecture_id: Integer, 강의 ID
        #   lecture_name: String, 강의명
        #   tutor_name: String, 강사명
        #   rate: Float, 점수
        #   comment: String, 강의평
      410:
        description : 실패
    """
    page = int(request.args.get('page', 1))
    data = model.recommendLectureComments(page=page)
    return make_response(json.jsonify(data=data), 200)

@login_required
def lectureNameAutoCompletion():
    """
    강의명 자동완성
    ---
    tags:
      - lectureList
    parameters:
      - name : school_id
        in : query
        required : true
        type : integer
        description : 학교 ID
      - name : keyword
        in : query
        required : true
        type : string
        description : 키워드의 앞 부분
    responses:
      200:
        description : 성공
        schema:
          id: candidate
          type: object
        examples :
          candidate : ['전기','전기공학부','전자공학부']
      410:
        description : 실패
    """
    partitioned_keyword = request.args.get('term')
    school_id = int(request.args.get('school_id', -1))
    if school_id == -1:
        return make_response("Invalid school_id", 410)
        
    cand = model.lectureNameAutoCompletion(school_id, partitioned_keyword)
    return make_response(json.jsonify(cand), 200)

@login_required
def lectureFind(school_id):
    """
    강의찾기
    ---
    tags:
      - lectureList
    parameters:
      - name : school_id
        in : path
        required : true
        type : integer
        description : 학교 ID
      - name : keyword
        in : query
        required : true
        type : string
        description : 키워드의 앞 부분
    responses:
      200:
        description : 성공
        # examples:
        #     “data”:[/* items */]
        #
        #     item 단위:
        #     article_id: Integer, 댓글 ID
        #     school_id: Integer, 학교 ID
        #     school_name: String, 학교 이름
        #     lecture_id: Integer, 강의 ID
        #     lecture_name: String, 강의명
        #     tutor_name: String, 강사명
        #     rate: Float, 점수
        #     text: String, 강의평
      410:
        description : 실패
    """
    keyword = request.args.get('keyword')
    data = model.lectureFind(school_id, keyword)
    return make_response(json.jsonify(data=data), 200)

