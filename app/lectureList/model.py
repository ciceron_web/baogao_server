from flask import g, session
from app.common import lib as commonLib


def newLectureComments(page=1):
    cursor = g.db.cursor()
    query = """
       SELECT 
           lec.lecture_id
         , fact.id as article_id
         , sch.id AS school_id
         , sch.name AS school_name
         , sch.color AS school_color
         , lec2.name AS lecture_name
         , lec2.professor as tutor_name
         , fact.rate
         , fact.text
       FROM
         (
           SELECT
               distinct lecture_id
             , max(created_at) AS created_at
             , max(id) AS article_id
           FROM article_detail_with_user
           WHERE text IS NOT null
           {school_id}
           GROUP BY lecture_id
           ORDER BY created_at DESC
           LIMIT 60
         ) lec
           LEFT OUTER JOIN lectures lec2
        ON lec.lecture_id = lec2.lecture_id
           LEFT OUTER JOIN school_id_relation rel
           	ON lec2.school_id = rel.other_service_school_id
           LEFT OUTER JOIN school sch
           	ON sch.id = rel.baogao_school_id
       LEFT OUTER JOIN article_detail_with_user fact
         ON     lec.lecture_id = fact.lecture_id
            AND lec.created_at = fact.created_at
    """
    #query = query.format(page= (page-1) * 12)
    if 'logged_in' in session and session.get('logged_in') == True:
        query = query.format(school_id='  AND school_id = %s' % session['school_id'])
    else:
        query = query.format(school_id='')
    cursor.execute(query)

    ret = cursor.fetchall()
    return ret

def recommendLectureComments(page=1):
    cursor = g.db.cursor()
    query = """
        SELECT
            school_id
          , school_name
          , lecture_id
          , lecture_name
          , professor AS tutor_name
          , rate
          , text AS comment
          , school_color
          , max(created_at) as created_at
        FROM article_detail_with_user
        WHERE text IS NOT null
          AND rate >= 3.5
          {school_id}
        GROUP BY school_id, lecture_id, lecture_name, professor, rate, text
	# ORDER BY created_at DESC LIMIT 12 OFFSET --page--
        ORDER BY RAND()
        LIMIT 60
    """
    #query = query.format(page= (page-1) * 12)
    if 'logged_in' in session and session.get('logged_in') == True:
        query = query.format(school_id='AND school_id = %s' % session['school_id'])
    else:
        query = query.format(school_id='')
    cursor.execute(query)

    ret = cursor.fetchall()
    return ret

def lectureNameAutoCompletion(school_id, partitioned_keyword):
    cursor = g.db.cursor()
    query = """
        SELECT distinct(lecture_name)
        FROM lecture_detail_with_user
        WHERE school_id = %s
          AND lecture_name LIKE '%%%s%%'
    """
    query = query % (school_id, partitioned_keyword)
    cursor.execute(query)
    ret = cursor.fetchall()
    data = [ item['lecture_name'] for item in ret ]
    return data

def lectureFind(school_id, keyword):
    cursor = g.db.cursor()
    query = """
    SELECT 
        lec.lecture_id
      , fact.id as article_id
      , sch.id AS school_id
      , sch.name AS school_name
      , sch.color AS school_color
      , lec2.name AS lecture_name
      , lec2.professor as tutor_name
      , fact.rate
      , fact.text
    FROM
      (
        SELECT
            distinct lecture_id
          , max(created_at) AS created_at
          , max(id) AS article_id
        FROM article_detail_with_user
        WHERE school_id = %s
          AND lecture_name LIKE '%%%s%%'
        GROUP BY lecture_id
      ) lec
	LEFT OUTER JOIN lectures lec2
     ON lec.lecture_id = lec2.lecture_id
	LEFT OUTER JOIN school_id_relation rel
		ON lec2.school_id = rel.other_service_school_id
	LEFT OUTER JOIN school sch
		ON sch.id = rel.baogao_school_id
    LEFT OUTER JOIN article_detail_with_user fact
      ON     lec.lecture_id = fact.lecture_id
         AND lec.created_at = fact.created_at

    """
    query = query % (school_id, keyword)
    cursor.execute(query)
    ret = cursor.fetchall()
    for item in ret:
        if item['text'] is None:
            item['text'] = '첫 강의평을 남겨보세요!'
            item['article_id'] = None

    return ret
