from app import app, versions
import app.lectureList.controller as lectureList

for ver in versions:
    app.add_url_rule('{}/newLectureComments'.format(ver), view_func=lectureList.newLectureComments, methods=['GET'])
    app.add_url_rule('{}/recommendedLecture'.format(ver), view_func=lectureList.recommendLectureComments, methods=['GET'])
    app.add_url_rule('{}/lecturename'.format(ver), view_func=lectureList.lectureNameAutoCompletion, methods=['GET'])
    app.add_url_rule('{}/school/<int:school_id>/lectureFind'.format(ver), view_func=lectureList.lectureFind, methods=['GET'])
