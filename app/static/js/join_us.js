$(document).ready(function () {
	var check_email, password, school_id, major, name, enter_year, phone, wechat_id, referred_by, birth;

	var regExp_mail = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; // mail check
	var regExp_phone = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/; // phone check
	var regExp_pass = /^[a-z0-9_]{4,20}$/; // pass check

	$("body").on("click", ".intro-logo img, .form-header-logo img, .form-header-back", function () {
		location.href = "/";
	});

	$("body").on("keyup", "#join-pass, #user_passwd_confirm", function () {
		if ($("#join-pass").val() == $("#user_passwd_confirm").val()) {
			$(".check_password").html("Matching").css("color", "green");
		} else {
			$(".check_password").html("Not Matching").css("color", "red");
		}
	});

	$("body").on("keyup", "#join-email", function () {
		// console.log($(this).val());
		if ($(this).val() == "") {
			$(".possible_mail").html("");
		}
	});

	$("body").on("click", ".check_email", function (e) {
		e.preventDefault();
		check_email = $("#join-email").val();

		if (check_email == "") {
			alert("이메일을 확인해주세요.");
		} else if (!regExp_mail.test($("#join-email").val())) {
			// console.log("메일 아냐");
			$(".possible_mail").html("");
			alert("이메일 형식이 맞지 않습니다.");
			return false;
		} else {
			$.ajax({
				url: "/api2/v2/idUniqueCheck?email=" + check_email,
				type: "get",
				contentType: false,
				processData: false,
			}).done(function (data) {
				// console.log(data);
				// alert("사용할 수 있습니다.");
				$(".possible_mail").html("사용 가능한 이메일입니다.");
			}).fail(function (err) {
				console.log(err);
				alert("사용 중인 이메일입니다.");
			});

		}

	});


	function checkPassword(email, password) {
		password = $("#join-pass").val();
		var checkNumber = password.search(/[0-9]/g);
		var checkEnglish = password.search(/[a-z]/ig);

		if (!/^[a-zA-Z0-9]{4,16}$/.test(password)) {
			alert('숫자와 영문자 조합으로 4~16자리를 사용해야 합니다.');
			return false;
		}
		if (checkNumber < 0 || checkEnglish < 0) {
			alert("숫자와 영문자를 혼용하여야 합니다.");
			return false;
		}
		if (/(\w)\1\1\1/.test(password)) {
			alert('같은 문자를 4번 이상 사용하실 수 없습니다.');
			return false;
		}
		if (password.search(email) > -1) {
			alert("비밀번호에 아이디가 포함되었습니다.");
			return false;
		}
		return true;
	}

	$("body").on("click", ".btnArea button.signup", function () {
		// console.log($(this).val());
		check_email = $("#join-email").val();
		if (checkPassword(check_email, password)) {
			if ($("#agree_service_check0")[0].checked != true) {
				alert("개인정보취급방침 및 이용약관을 동의해주세요.");
			} else {
				signup();
				return false;
			}

		}
	});

	var signup = function () {
		password = $("#join-pass").val();
		password = $.sha256(password);
		school_id = $("#join-school").val();
		major = $("#major").val();
		name = $("#name").val();
		enter_year = $("input[name='school-year']").val();

		phone = $("#mobile1").val();
		phone += $("#mobile2").val();
		phone += $("#mobile3").val();

		wechat_id = $("#weChat-id").val();
		referred_by = $("input[name='recommender']").val();

		birth = $("#birth_year").val();

		var formData = new FormData();
		formData.append("email", check_email);
		formData.append("password", password);
		formData.append("school_id", school_id);
		formData.append("major", major);
		formData.append("name", name);
		formData.append("enter_year", enter_year);
		formData.append("phone", phone);
		if (wechat_id != "") {
			formData.append("wechat_id", wechat_id);
		}
		if (referred_by != "") {
			formData.append("referred_by", referred_by);
		}
		if (birth != "") {
			formData.append("birth", birth);
		}

		$.ajax({
			url: "/api2/v2/signUp",
			type: "POST",
			dataType: "text",
			contentType: false,
			processData: false,
			data: formData
		}).done(function (data) {
			console.log(data);
			alert("가입되었습니다. 환영합니다~");
			location.href = "/evaluation2";
		}).fail(function (err) {
			console.log(err);
		});
	}

});

