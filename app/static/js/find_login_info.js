$(document).ready(function () {
    var email;
    $("body").on("click", ".login_btn", function(){
        location.href="/evaluation2/login";
    });

    $("body").on("click", ".join_btn", function(){
        location.href="/evaluation2/joinus";
    });

    $("body").on("click", ".find_btn", function(){
        email = $("input[name='email']").val();
        var formData = new FormData();
        formData.append("email", email);

        $.ajax({
            url: "/api2/v2/sendPasswordResetMail",
            type: "POST",
            // dataType: "JSON",
            contentType: false,
            processData: false,
            data: formData
        }).done(function (data) {
            // console.log(data);
            alert("임시 비밀번호가 메일로 발급되었습니다.");
            location.href = "/evaluation2/login";
        }).fail(function (err) {
            console.log(err);
        });

        return false;
    });
});

