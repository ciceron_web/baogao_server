import traceback
from flask import g, session
from app.common import lib as commonLib


def seeDetailComment(school_id, lecture_id):
    cursor = g.db.cursor()
    # 수정할것
    query_lecture = """
        SELECT
            school_id
          , school_name
          , lecture_id
          , lecture_name
          , professor AS tutor_name
        FROM article_detail_with_user
        WHERE school_id = %s
          AND lecture_id = %s
        LIMIT 1
    """
    cursor.execute(query_lecture, (school_id, lecture_id, ))
    ret = cursor.fetchone()
    data_common = ret

    query = """
        SELECT
            id as article_id
          , school_id
          , school_name
          , lecture_id
          , lecture_name
          , professor
          , rate
          , user_id
          , user_email
          , text
        FROM article_detail_with_user
        WHERE school_id = %s
          AND lecture_id = %s
          AND text is not null
    """
    cursor.execute(query, (school_id, lecture_id, ))
    ret = cursor.fetchall()

    data = []
    for item in ret:
        unit = {}
        unit['article_id'] = item['article_id']
        unit['rate'] = item['rate']
        unit['comment'] = item['text']
        unit['is_yours'] = True if item['user_email'] == session.get('email') else False
        unit['masked_email'] = item['user_email'][:3].ljust(12, "*") if item.get('user_email') is not None else 'Null somebody'

        data.append(unit)

    return data_common, data

def inputDetail(school_id, lecture_id, **kwargs):
    cursor = g.db.cursor()
    other_service_school_id = commonLib.getCrawledSchoolId(g.db, school_id)
    query = """
        INSERT INTO articles
          (school_id, lecture_id, year, semester, user_id, rate, translated, created_at)
        VALUES
          (%s,        %s,         %s,   %s,       %s,      %s,   %s,         current_timestamp)
    """
    user_id = commonLib.get_user_id(g.db, session['email'])
    try:
        cursor.execute(query,
                (other_service_school_id,
                 lecture_id,
                 kwargs['year'],
                 kwargs['semester'],
                 user_id,
                 kwargs['rate'],
                 kwargs['comment'],
                )
                      )
        g.db.commit()

    except:
        traceback.print_exc()
        g.db.rollback()
        return False

    return True

def updateDetail(article_id, **kwargs):
    cursor = g.db.cursor()
    user_id = commonLib.get_user_id(g.db, session['email'])

    query_checkUser = """
        SELECT user_id
        FROM articles
        WHERE article_id = %s
    """
    cursor.execute(query_checkUser, (article_id, ))
    ret = cursor.fetchone()
    if ret is None or len(ret) == 0:
        return False

    article_userId = ret['user_id']
    if article_userId != user_id:
        return False

    for key in kwargs.keys():
        if key not in ["year", "semester", "rate", "translated"]:
            return False

    query_update = """
        UPDATE articles
        SET {value}
        WHERE article_id = %s
    """

    params = []
    values = []
    for key, value in kwargs.items():
        params.append("{}=%s".format(key))
        values.append(value)

    try:
        query_update = query_update.format(value=", ".join(params))
        values.append(article_id)
        cursor.execute(query_update, values)
        g.db.commit()

    except Exception:
        traceback.print_exc()
        g.db.rollback()
        return False

    return True

def deleteArticle(article_id):
    cursor = g.db.cursor()
    user_id = commonLib.get_user_id(g.db, session['email'])

    query = """
        DELETE FROM articles
        WHERE article_id = %s AND user_id = %s
    """
    try:
        cursor.execute(query, (article_id, user_id, ))
        g.db.commit()

    except:
        traceback.print_exc()
        conn.rollback()
        return False

    return True

def provideCommentsForTranslation():
    cursor = g.db.cursor()
    query = """
        SELECT lecture_id, article_id, text
        FROM articles
        WHERE translated IS null
        ORDER BY rand()
        LIMIT 3
    """
    cursor.execute(query)
    ret = cursor.fetchall()
    data = []
    for item in ret:
        unit = {}
        unit['lecture_id'] = item['lecture_id']
        unit['article_id'] = item['article_id']
        unit['text'] = item['text']

        data.append(unit)

    return data

def inputTranslation(lecture_id, article_id, translated_text):
    cursor = g.db.cursor()
    query = """
        UPDATE articles
        SET
          translated = %s,
          created_at = CURRENT_TIMESTAMP,
          user_id = %s
        WHERE
              article_id = %s
          AND lecture_id = %s
    """
    user_id = commonLib.get_user_id(g.db, session.get('email', None))
    try:
        cursor.execute(query, (translated_text, user_id, article_id, lecture_id, ))
        g.db.commit()

    except:
        g.db.rollback()
        traceback.print_exc()
        return False

    return True
