from app import app, versions
import app.lectureDetail.controller as lectureDetail

for ver in versions:
    app.add_url_rule('{}/school/<int:school_id>/lecture/<int:lecture_id>'.format(ver), view_func=lectureDetail.seeDetailComment, methods=['GET'])
    app.add_url_rule('{}/school/<int:school_id>/lecture/<int:lecture_id>'.format(ver), view_func=lectureDetail.inputDetail, methods=['POST'])
    app.add_url_rule('{}/school/<int:school_id>/lecture/<int:lecture_id>/article/<int:article_id>'.format(ver), view_func=lectureDetail.updateArticle, methods=['PUT'])
    app.add_url_rule('{}/school/<int:school_id>/lecture/<int:lecture_id>/article/<int:article_id>'.format(ver), view_func=lectureDetail.deleteArticle, methods=['DELETE'])
    app.add_url_rule('{}/untranslated'.format(ver), view_func=lectureDetail.provideCommentsForTranslation, methods=['GET'])
    app.add_url_rule('{}/inputTranslate'.format(ver), view_func=lectureDetail.inputTranslation, methods=['POST'])

