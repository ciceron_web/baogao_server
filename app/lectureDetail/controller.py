from flask import request, json, make_response
from . import model
from app.common import lib as commonLib
from app.common.lib import login_required
from app.common.lib import cralwer_blocker


@login_required
@cralwer_blocker
def seeDetailComment(school_id, lecture_id):
    """
    강의평 리스트 보기
    ---
    tags:
        - lectureDetail
    parameters:
      - name : school_id
        in : path
        description : 학교 ID
        required: true
        type : integer
      - name : lecture_id
        in : path
        description : 강의 ID
        required: true
        type : integer
    responses:
      200:
        description: OK
        schema:
          id: data
          type: object
        examples:
          school_id: Integer, 학교 ID
          school_name: String, 학교 이름
          lecture_id: Integer, 강의 ID
          lecture_name: String, 강의명
          tutor_name: String, 강사명
          rate: Float, 점수
          comment: String, 강의평
      410:
        description: Fail
    """
    data_common, data = model.seeDetailComment(school_id, lecture_id)
    return make_response(json.jsonify(common=data_common, data=data), 200)

@login_required
@cralwer_blocker
def inputDetail(school_id, lecture_id):
    """
    강의평 입력
    ---
    tags:
      - lectureDetail
    parameters:
      - name : school_id
        in : path
        description : 학교 ID
        required: true
        type : integer
      - name : lecture_id
        in : path
        description : 강의 ID
        required: true
        type : integer
      - name : year
        in : formData
        description : 수업 년도
        required: true
        type : integer
      - name : semester
        in : formData
        description :
          - 학기
          - 1 - 1학기
          - 2 - 2학기
          - S - 여름계절학기
          - W - 겨울계절학기
        enum: [1,S,2,W]
        required: true
        type : string
      - name : rate
        in : formData
        description : 점수
        required: true
        type : number
      - name : comment
        in : formData
        description : 강의평
        required: true
        type : string
    responses:
      200:
        description: OK
      410:
        description: Fail
    """
    params = {
                'year': request.form['year']
              , 'semester': request.form['semester']
              , 'rate': request.form['rate']
              , 'comment': request.form['comment']
             }

    is_ok = model.inputDetail(school_id, lecture_id, **params)

    if is_ok == True:
        return make_response("OK", 200)
    else:
        return make_response("Fail", 410)

@login_required
def provideCommentsForTranslation():
    """
    미번역 강의평 3개 랜덤 추출
    ---
    tags:
      - lectureDetail
    responses:
      200:
        description: OK
        schema:
          id: data
          type: object
        examples:
          data :
            lecture_id: Int, 강의 ID
            article_id: Int, 강의평 ID
            text: String, 텍스트
      410:
        description: Fail
    """
    data = model.provideCommentsForTranslation()
    return make_response(json.jsonify(data=data), 200)

@cralwer_blocker
def inputTranslation():
    """
    강의평 번역하기
    ---
    tags:
      - lectureDetail
    parameters:
      - name : lecture_id
        in : formData
        description : 강의 ID
        required: true
        type : integer
      - name : article_id
        in : formData
        description : 강의평 ID
        required: true
        type : integer
      - name : text
        in : formData
        description : 텍스트
        required: true
        type : string
    responses:
      200:
        description: OK
      410:
        description: Fail
    """
    article_id = request.form['article_id']
    lecture_id = request.form['lecture_id']
    translated_text = request.form['text']

    is_ok = model.inputTranslation(lecture_id, article_id, translated_text)
    if is_ok == True:
        return make_response("OK", 200)

    else:
        return make_response("Fail", 410)

@cralwer_blocker
def updateArticle(school_id, lecture_id, article_id):
    """
    강의평 수정
    ---
    tags:
      - lectureDetail
    parameters:
      - name : text
        in : formData
        description : 텍스트(Optional)
        required: false
        type : integer
      - name : year
        in : formData
        description : 년도(Optional)
        required: false
        type : integer
      - name : semester
        in : formData
        description : 학기(Optional, := [1, 2, S, W])
        required: false
        type : string
      - name : rate
        in : formData
        description : 별점(Optional)
        required: false
        type : float
    responses:
      200:
        description: OK
      410:
        description: Fail
    """
    params = {}
    for key, value in request.form.items():
        if key == "text":
            params['translated'] = value
        else:
            params[key] = value

    is_ok = model.updateDetail(article_id, **params)
    if is_ok == True:
        return make_response("OK", 200)

    else:
        return make_response("Fail", 410)

@cralwer_blocker
def deleteArticle(school_id, lecture_id, article_id):
    """
    강의평 삭제
    ---
    tags:
      - lectureDetail
    responses:
      200:
        description: OK
      410:
        description: Fail
    """
    is_ok = model.deleteArticle(article_id)
    if is_ok == True:
        return make_response("OK", 200)

    else:
        return make_response("Fail", 410)

