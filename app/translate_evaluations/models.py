from flask import g
import traceback
import pymysql

def selectArticles(school_id):
    curs = g.db.cursor()
    print(school_id)
    sql = """
                      select articles.who, articles.article_id, lectures.name, lectures.professor, text, articles.translated,lectures.school_id 
                      from lectures,articles 
                      where lectures.lecture_id = articles.lecture_id and lectures.school_id=%s 
                      order by lectures.name, lectures.lecture_id
                    """
    print(sql)
    curs.execute(sql, (school_id))
    entries = curs.fetchall()

    return entries

def selectArticlesWithSearchItem(school_id,search_item):
    curs = g.db.cursor()
    sql = """
                          select articles.who, articles.article_id, lectures.name, lectures.professor, text, articles.translated,lectures.school_id 
                          from lectures,articles 
                          where lectures.lecture_id = articles.lecture_id and lectures.school_id=%s and name like %s
                          order by lectures.name, lectures.lecture_id
                        """ % (school_id, search_item)
    curs.execute(sql)
    entries = curs.fetchall()
    print(sql)

    return entries

def selectArticlesWithTranslator(school_id,search_item):
    curs = g.db.cursor()
    sql = """
                          select articles.who, articles.article_id, lectures.name, lectures.professor, text, articles.translated,lectures.school_id 
                          from lectures,articles 
                          where lectures.lecture_id = articles.lecture_id and lectures.school_id=%s and who like %s
                          order by lectures.name, lectures.lecture_id
                        """ % (school_id, search_item)
    curs.execute(sql)
    entries = curs.fetchall()
    print(sql)

    return entries

def updateTranslation(text,who,article_id):
    curs = g.db.cursor()
    sql = "update articles set translated=%s,who=%s where article_id=%s"
    try:
        curs.execute(sql, (text, who, article_id))
    except:
        traceback.print_exc()
        # conn.rollback()

    g.db.commit()
