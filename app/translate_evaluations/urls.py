from app import app,versions
import app.translate_evaluations.controllers as te
from flask.views import View

for version in versions:
    #app.add_url_rule('/evaluation'.format(version), view_func=te.show_schools , methods=["GET"])
    #app.add_url_rule('/evaluation'.format(version), view_func=te.select_school , methods=["POST"])
    app.add_url_rule('/evaluation/schools'.format(version), view_func=te.show_entries, methods = ['GET','POST'])
    app.add_url_rule('/evaluation/schools/articles'.format(version), view_func=te.add_entry, methods=['POST'])

    # app.add_url_rule('/about', view_func=RenderTemplateView.as_view('about_page', template_name='about.html'))
    # app.add_url_rule('{}/test'.format(version), view_func=View.as_view('select'))
