from flask import Flask, render_template, request, redirect, url_for
import app.translate_evaluations.models as TE

def show_schools():
    """
    15개 학교 리스트 보여주기
    ---
    tags:
        - Translate Evaluation
    """
    return render_template('select.html')

def select_school():
    """
    사용자가 선택한 학교 데이터 받기
    ---
    tags:
        - Translate Evaluation
    parameters:
      - name: school_id
        in: formData
        description: 학교별로 부여된 id
        required: true
        type: int32
    """
    school_id = request.form['school_id']
    return redirect(url_for('show_entries', school_id=school_id))

def show_entries():
    """
    조건에 해당되는 강의평 리스트 보여주기
    ---
    tags:
        - Translate Evaluation
    produces:
        - text/html
    parameters:
        - name: school_id
          in: query
          description: 학교별로 부여되는 id
          required: true
          type: int32
        - name: search
          in: formData
          description:
              - 검색한 단어
              - GET 으로 요청할때는 필요없음
          type: string
    definitions:
      Data:
        type: object
        properties:
          who:
            type: string
            description: 번역가명
          name:
            type: string
            description: 강의명
          professor:
            type: string
            description: 교수명
          text:
            type : string
            description : 강의평 원문
          translated:
            type : string
            description : 번역된 강의평
          school_id:
            type : int32
            description : 학교마다 부여된 id
    responses:
      200:
        description:
          - 데이터베이스에서 조건에따라 검색한 결과들
        schema:
          id: Data
          type: object
        examples:
          data: [ {'who': '王佳琳', 'article_id': 14619, 'name': '4D컴퓨터그래픽(영강)',
          'professor': '유승헌', 'lecture_id': 320977, 'Articles.article_id': 14619,
          'text': '좋은강ㅇ였습니다.플래시에대해 잘배울수있는 강의입니다!', 'translated': '很好的课，可以学好flash', 'school_id': 22} ]
    """
    school_id = request.values.get('school_id',None)
    print(school_id)
    search_word= request.values.get('search', None)

    if(search_word):
        search_item = "'%" + search_word + '%' + "'"
        entries = TE.selectArticlesWithSearchItem(school_id,search_item)

    elif(search_word==None or search_word==''):
        entries = TE.selectArticles(school_id)

    translator = request.values.get('translator',None)
    if(translator):
        search_item = "'%" + translator + '%' + "'"
        entries = TE.selectArticlesWithTranslator(school_id,search_item)

    #여기서 검색결과가 하나도 없어서 entreis가 0으로 나왔을때 처리
    if(entries==()):
        entries = [{'who': '', 'article_id': 0, 'name': '', 'professor': '', 'text': '검색결과가 없습니다',
                    'translated': '', 'school_id': school_id}]

    return render_template('show_entries.html',entries=entries)

# @app.route('/evaluation/articles', methods=['POST']) # /evaluation/schools/<int:school_id>/articles/<int:article_id>
def add_entry():
    """
    새로운 번역 등록하기
    ---
        tags:
          - Translate Evaluation
        parameters:
          - name: translated
            in: formData
            description: 번역된 강의평
            type: string
            required: true
          - name: who
            in: formData
            description : 번역가명
            type: string
            required: true
          - name: article_id
            in: formData
            description: 번역된 강의평의 id
            type: int32
            required: true
          - name: school_id
            in: formData
            description: 번역된 강의평의 학교 id
            type: int32
            required: true
          - name: name
            in: formData
            description: 이전에 번역된 강의평의 이름. redirect할 때 검색어로 넘겨줌
            type: string
            required: true

        responses:
          200:
            description: 업데이트 성공
          400:
            description: 업데이트 실패
    """

    TE.updateTranslation(request.form['translated'], request.form['who'], request.form['article_id'])

    school_id = request.form['school_id']
    lectrue_name = request.form['name']

    return redirect(url_for('show_entries', school_id=school_id, search=lectrue_name))
