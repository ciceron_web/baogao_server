# -*- coding: utf-8 -*-
#: 공통으로 사용하는 것 모음
import string, random
import traceback
import hashlib
from functools import wraps
from flask import session, make_response, json, abort, request, g


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('logged_in') == True :
            return f(*args, **kwargs)

        else:
            return abort(403)

    return decorated_function

def cralwer_blocker(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        cursor = g.db.cursor()

        if session.get('logged_in') is not None:
            user_id = get_user_id(g.db, session['email'])
        else:
            user_id = 0

        method = request.method
        api_endpoint = request.environ['PATH_INFO']
        ip_address = request.headers.get('x-forwarded-for-client-ip')

        query_apiCount = """
            SELECT
                  method
                , api
                , count(*) AS cnt
            FROM temp_action_log
            WHERE (user_id = %s OR ip_address = %s)
              AND log_time BETWEEN (CURRENT_TIMESTAMP - interval 1 minute) AND CURRENT_TIMESTAMP
              AND api != '/api2/v2'
            GROUP BY method, api
            ORDER BY cnt DESC
              """
        cursor.execute(query_apiCount, (user_id, ip_address, ))
        ret = cursor.fetchone()
        if ret is not None:
            conn_count = ret['cnt']
        else:
            conn_count = 0

        query_getBlacklist = """
            SELECT count(*) AS cnt
            FROM blacklist
            WHERE (user_id = %s OR ip_address = %s)
              AND CURRENT_TIMESTAMP BETWEEN time_from AND time_to
        """
        cursor.execute(query_getBlacklist, (user_id, ip_address, ))
        blacklist_count = cursor.fetchone()['cnt']

        if conn_count > 30:
            keys = list(session.keys())
            for key in keys:
                session.pop(key)

            query_insertBlackList = """
                INSERT INTO blacklist (user_id, ip_address, time_from, time_to)
                VALUES
                (
                   %s
                  ,%s
                  ,CURRENT_TIMESTAMP
                  ,CURRENT_TIMESTAMP + interval 2 HOUR
                )
            """
            try:
                cursor.execute(query_insertBlackList, (user_id, ip_address, ))
                g.db.commit()

            except:
                traceback.print_exc()
                g.db.rollback()

            return abort(503)

        if blacklist_count > 0:
            keys = list(session.keys())
            for key in keys:
                session.pop(key)

            return abort(503)

        query_insertLog = """
            INSERT INTO temp_action_log
              (user_id, method, api, log_time, ip_address)
            VALUES
              (
                 %s
                ,%s
                ,%s
                ,CURRENT_TIMESTAMP
                ,%s
              )
        """
        try:
            cursor.execute(query_insertLog, (user_id, method, api_endpoint, ip_address, ))
            g.db.commit()

        except:
            g.db.rollback()
            traceback.print_exc()
            return abort(503)

        return f(*args, **kwargs)

    return decorated_function

def _insert(conn, table, **kwargs):
    cursor = conn.cursor()
    query_tmpl = """
        INSERT INTO {table}
        ({columns})
        VALUES
        ({prepared_statements})
    """
    columns = ','.join( list( kwargs.keys() ) )
    prepared_statements = ','.join( ['%s' for _ in list(kwargs.keys())] )
    query = query_tmpl.format(
                table=table
              , columns=columns
              , prepared_statements=prepared_statements
              )

    try:
        cursor.execute(query, list( kwargs.values() ) )
        conn.commit()

    except Exception:
        traceback.print_exc()
        conn.rollback()
        return False

    return True

def _update(conn, table, something_id, **kwargs):
    cursor = conn.cursor()
    query = """
        UPDATE {table}
        SET {value}
        WHERE id = %s
    """

    params = []
    values = []
    for key, value in kwargs.items():
        params.append("{}=%s".format(key))
        values.append(value)

    try:
        query = query.format(
                  table=table
                , value=", ".join(params)
                )
        values.append(something_id)
        cursor.execute(query, values)
        conn.commit()

    except Exception:
        traceback.print_exc()
        conn.rollback()
        return False

    return True

def _delete(conn, table, something_id):
    cursor = conn.cursor()
    query = """
        DELETE FROM {table}
        WHERE id = %s
    """

    try:
        query = query.format(table=table)
        cursor.execute(query, (something_id, ))
        conn.commit()

    except Exception:
        traceback.print_exc()
        conn.rollback()
        return False

    return True

def random_string_gen(size=6, chars=None):
    """
    무작위 string 만들어줌. 길이 조절도 가능함
    """
    try:
        chars = string.ascii_letters + string.digits
    except:
        chars = string.letters + string.digits

    gened_string = ''.join(random.choice(chars) for _ in range(size))
    return gened_string

def get_hashed_password(password, salt=None):
    """
    비밀번호 hashing 라이브러리. Salt 넣으면 그것도 반영하여 만들어줌
    """
    hash_maker = hashlib.sha256()

    if salt is None:
        hash_maker.update(password.encode('utf-8'))
    else:
        hash_maker.update(salt.encode('utf-8') + password.encode('utf-8') + salt.encode('utf-8'))

    return hash_maker.hexdigest()

def get_user_id(conn, text_id):
    """
    이메이을 넣으면 해당 사용자의 ID를 찾아줌
    email은 indexing 해놨기때문에 성능상의 큰 문제는 없을것임
    """
    cursor = conn.cursor()
    cursor.execute("SELECT id from users WHERE email = %s", (text_id, ))
    rs = cursor.fetchone()
    if rs is None or len(rs) == 0: result = -1
    else:            result = int(rs['id'])
    return result

def getCrawledSchoolId(conn, baogao_id):
    cursor = conn.cursor()
    cursor.execute("SELECT other_service_school_id from school_id_relation WHERE baogao_school_id = %s", (baogao_id, ))
    rs = cursor.fetchone()
    if rs is None or len(rs) == 0: result = -1
    else:            result = int(rs['other_service_school_id'])
    return result

def sendMail(mail_to, subject, message, mail_from='no-reply@ciceron.me'):
    """
    주어진 mesasge를 메일로 날려주는 라이브러리이다.
    """
    import smtplib
    import base64
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    try:
        content = MIMEText(message, 'html', _charset='utf-8')
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = 'CICERON team <%s>' % mail_from
        msg['To'] = str(mail_to)
        msg.attach(content)

        print(msg)
        a = smtplib.SMTP('smtp.gmail.com:587')
        a.ehlo()
        a.starttls()
        a.login('no-reply@ciceron.me', 'ciceron3388!')
        a.sendmail('no-reply@ciceron.me', str(mail_to), msg.as_string())
        a.quit()

    except:
        return False

    return True

def dbToDict(columns, ret):
    result = [] 
    if ret is None or len(ret) == 0:
        return result

    for row in ret: 
        item = { columns[idx]:col for idx, col in enumerate(row) }
        result.append(item)

    return result
