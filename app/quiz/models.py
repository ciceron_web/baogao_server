import traceback, psycopg2

try:
    DATABASE = "host=aristoteles.ciceron.xyz port=5432 dbname=ciceron user=ciceron_web password=noSecret01!"
    conn = psycopg2.connect(DATABASE)
    print('Quiz DB Connection Success!')
except:
    print('Quiz DB Connection Error!')

def makeObjectiveQuiz(quiz_type):
    cursor = conn.cursor()
    query_select_quiz = """
            SELECT id, original_language_id, original_{quiz_type}, translated_{quiz_type}
            FROM ciceron.{quiz_type}s
            WHERE (original_language_id = 1 AND target_language_id = 4) OR (original_language_id = 4 AND target_language_id = 1)
            ORDER BY RANDOM() LIMIT 2;
            """
    data = {}

    cursor.execute(query_select_quiz.format(quiz_type=quiz_type))
    result = cursor.fetchall()
    data['id'] = result[0][0]
    if result[0][1] is 1:
        # 한국어가 original일 경우
        data['quiz'] = result[0][2]
        data['correct_answer'] = result[0][3]
        data['wrong_answer'] = result[1][3]
    else:
        # 한국어가 translate인 경우
        data['quiz'] = result[0][3]
        data['correct_answer'] = result[0][2]
        data['wrong_answer'] = result[1][2]
    return data

def makeSubjectiveQuiz():
    cursor = conn.cursor()
    query_select_quiz = """
    SELECT id, translated_sentence, original_sentence 
    FROM ciceron.sentences
    WHERE (original_language_id = 1 AND target_language_id = 4 AND cardinality(regexp_split_to_array(original_sentence, '\s')) < 5)
          OR (original_language_id = 4 AND target_language_id = 1 AND cardinality(regexp_split_to_array(translated_sentence, '\s')) < 5)
    ORDER BY RANDOM() LIMIT 2;
    """
    data = {}

    cursor.execute(query_select_quiz)
    result = cursor.fetchall()
    data['id'] = result[0][0]
    data['quiz'] = result[0][1]

    # 보기 답안 띄어쓰기로 나누기
    correct_answer = result[0][2]
    wrong_answer = result[1][2]
    data['correct_answer'] = correct_answer.split()
    data['wrong_answer'] = wrong_answer.split()
    return data

def updateQuizCnt(quiz_type, result, dataId):
    cursor = conn.cursor()
    query_update_cnt_fail = """
            update ciceron.{}s
            set quiz_submit_cnt = quiz_submit_cnt + 1
            where id = %s;
            """
    query_update_cnt_pass = """
            update ciceron.{}s
            set quiz_submit_cnt = quiz_submit_cnt + 1, quiz_correct_cnt = quiz_correct_cnt + 1
            where id = %s;
            """

    try:
        if result == 'pass':
            cursor.execute(query_update_cnt_pass.format(quiz_type), (dataId,))
        elif result == 'fail':
            cursor.execute(query_update_cnt_fail.format(quiz_type), (dataId,))

        if cursor.rowcount == -1 or cursor.rowcount == 0:
            conn.rollback()
            return False
        conn.commit()
    except:
        traceback.print_exc()
        conn.rollback()
        return False
    return True

