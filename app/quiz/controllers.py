from flask import session, request, make_response, json

from app.common import lib
import app.quiz as q
import app.quiz.models as Quiz

def start_quiz():
    """
    퀴즈 시작
    ---
    tags:
      - Quiz
    responses:
      200:
        description: 퀴즈 시작 성공
        examples:
          message: 'Quiz Start!'
      400:
        description: 퀴즈 시작 실패
        examples:
          message: 'Something Wrong!'
    """
    # 세션값으로 임의의 값 삽입
    session['username'] = lib.random_string_gen()
    q.username = session['username']

    # 퀴즈 1~4번 패스 확인 변수를 False로 초기화 및 세션에 삽입
    for i in range(len(q.quiz_type)):
        quiz_number = 'quiz{}'.format(i + 1)
        session[quiz_number] = False
    return make_response(json.jsonify(message='Quiz Start!'), 200)

def end_quiz():
    """
    퀴즈 종료
    ---
    tags:
      - Quiz
    responses:
      200:
        description: 퀴즈 종료 성공
        examples:
          message: 'Quiz End!'
      400:
        description: 퀴즈 종료 실패. 퀴즈를 다 풀지 않은 경우
        examples:
          message: 'Solve all quiz? Please Check!'
    """
    # session의 퀴즈 1~4번 패스 확인 변수가 전부 True인지 확인
    for i in range(len(q.quiz_type)):
        quiz_number = 'quiz{}'.format(i + 1)
        if session[quiz_number] != True:
            return make_response(json.jsonify(message='퀴즈를 다 풀지 않았습니다'), 410)
        else:
            session.pop(quiz_number, None)

    # 세션에서 username 삭제
    if q.username == session['username']:
        session.pop('username', None)
        q.username = ''
    else:
        return make_response(json.jsonify(message='도전자의 username이 일치하지 않습니다.'), 410)

    return make_response(json.jsonify(message='Quiz End!'), 200)

def make_quiz(quiz_id):
    """
    퀴즈 출제
    ---
    tags:
      - Quiz
    parameters:
      - name: quiz_id
        in: path
        description:
          - 퀴즈 형식 번호
          - 1 - 객관식, 명사
          - 2 - 객관식, 동사
          - 3 - 객관식, 문장
          - 4, 5 - 주관식, 문장
        enum: [1,2,3,4,5]
        required: true
        type: int32
    definitions:
      Data:
        type: object
        properties:
          data_id:
            type: int32
          quiz:
            type: string
          correct_answer:
            type: string
          wrong_answer:
            type: string
    responses:
      200:
        description:
          - correct_answer&wrong_answer - 1~3번(객관식)은 string, 4~5번(주관식)은 array
        schema:
          id: Data
          type: object
          properties:
            users:
              type: object
              items:
                $ref: '#/definitions/Data'
        examples:
          data: {'data_id': 1, 'quiz': "금융", 'correct_answer': "金融", 'wrong_answer': "脉搏数"}
    """
    # quiz_id가 문제 타입들 중에 있는가 확인, 1~5번 사이에 있는가 확인
    if quiz_id not in q.quiz_type.keys():
        return make_response(json.jsonify(message='올바른 quiz_id가 아닙니다'), 410)

    # 이전 문제 풀었나 확인하기
    before_quiz = 'quiz{}'.format(int(quiz_id) - 1)
    if quiz_id != 1 and session[before_quiz] == False:
        return make_response(json.jsonify(message='이전 문제를 풀지 않았습니다'), 410)

    # 문제 출제하기
    if quiz_id in range(4, 6):
        # 주관식 퀴즈 출제
        quiz = Quiz.makeSubjectiveQuiz()
    else:
        # 객관식 퀴즈 출제
        quiz = Quiz.makeObjectiveQuiz(q.quiz_type[quiz_id])
    return make_response(json.jsonify(data=quiz))

def submit_answer(quiz_id):
    """
    문제 결과 받기
    ---
    tags:
      - Quiz
    parameters:
      - name: quiz_id
        in: path
        description:
          - 문제 번호
          - 1 - 객관식, 명사
          - 2 - 객관식, 동사
          - 3 - 객관식, 문장
          - 4, 5 - 주관식, 문장
        enum: [1,2,3,4,5]
        required: true
        type: int32
      - name: result
        in: query
        description: pass/fail
        default: fail
        required: true
        type: string
      - name: dataId
        in: query
        description: 문제 출력시 나온 data_id
        required: true
        type: int32
    responses:
      200:
        description: 성공
      410:
        description: 실패, 세션값이 문제시작때 발급됐을때와 다른 경우
    """
    # quiz_id가 문제 타입들 중에 있는가 확인, 1~5번 사이에 있는가 확인
    if quiz_id not in q.quiz_type.keys():
        return make_response(json.jsonify(message='올바른 quiz_id가 아닙니다'), 410)

    # 세션의 username이 바뀌었나 확인
    if q.username != session['username']:
        return make_response(json.jsonify(message='세션의 username이 일치하지 않습니다'), 410)

    result = request.args.get('result', None)
    dataId = request.args.get('dataId', None)

    # 파라미터 값이 다 들어왔나 확인
    if not result or not dataId:
        return make_response(json.jsonify(message='result 또는 dataId 값이 없습니다'), 410)

    is_updated = Quiz.updateQuizCnt(q.quiz_type[quiz_id], result, dataId)
    if is_updated is True:
        # 다음 퀴즈로 넘어갈 수 있도록 세션 퀴즈 상태값을 바꿔준다
        quiz_number = 'quiz{}'.format(quiz_id)
        session[quiz_number] = True
        return make_response(json.jsonify(message='Go!'))
    else:
        return make_response(json.jsonify(message='Stop!'), 410)

def send_error():
    """
    관리자에게 오류 전송
    ---
    tags:
      - Quiz
    consumes:
      - application/x-www-form-urlencoded
    produces:
      - application/x-www-form-urlencoded
    parameters:
      - name: quiz
        in: formData
        description: 출제된 문제
        required: true
        type: string
      - name: correct_answer
        in: formData
        description: 정답
        required: true
        type: string
      - name: wrong_answer
        in: formData
        description: 오답
        required: true
        type: string
      - name: msg
        in: formData
        description: 관리자에게 보내는 메세지
        required: true
        type: string
    responses:
      200:
        description: 성공
      410:
        description: 실패
    """
    mail_data = {
        'quiz': request.form.get('quiz', None),
        'correct_answer': request.form.get('correct_answer', None),
        'wrong_answer': request.form.get('wrong_answer', None),
        'msg': request.form.get('msg', None)
    }
    # 파라미터 값들이 다 들어왔나 확인
    if None in mail_data.values():
        return make_response(json.jsonify(message='quiz, answer, msg 중 채워지지 않은 값이 있습니다'), 410)

    # 관리자에게 보낼 메일 형식
    mail_content = """
    사용자가 푼 QUIZ: {quiz}<br>
    정답 : {correct_answer}<br>
    오답 : {wrong_answer}<br>
    사용자가 보낸 내용: {msg}
    """.format(**mail_data)

    try:
        lib.sendMail(
            'betty@ciceron.me',
            'Found an error in the BaoGao Quiz!',
            mail_content
        )
    except:
        return make_response(json.jsonify(message='Send Mail Failure'), 410)
    return make_response(json.jsonify(message='Send Mail Success'), 200)

def check_session():
    """
    세션 확인
    서버에서 테스트 확인용으로 만든거에요, 사용하지 않습니다!
    ---
    tags:
      - Quiz
    deprecated:
      - False
    """
    sess = dict(session)
    return make_response(json.jsonify(session=sess, username=q.username))
