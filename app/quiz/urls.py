from app import app, versions
import app.quiz.controllers as quiz

for ver in versions:
    app.add_url_rule('{}/quiz/start'.format(ver), view_func=quiz.start_quiz, methods=['GET'])
    app.add_url_rule('{}/quiz/end'.format(ver), view_func=quiz.end_quiz, methods=['GET'])
    app.add_url_rule('{}/quiz/<int:quiz_id>'.format(ver), view_func=quiz.make_quiz, methods=['GET'])
    app.add_url_rule('{}/quiz/<int:quiz_id>/submit'.format(ver), view_func=quiz.submit_answer, methods=['POST'])
    app.add_url_rule('{}/quiz/sendError'.format(ver), view_func=quiz.send_error, methods=['POST'])
    app.add_url_rule('{}/quiz/check'.format(ver), view_func=quiz.check_session, methods=['GET'])

