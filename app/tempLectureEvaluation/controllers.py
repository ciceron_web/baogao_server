from flask import Flask, render_template, request, redirect, url_for, session, g
import app.tempLectureEvaluation.models as tempLE
from app.common.lib import login_required
from app.common.lib import cralwer_blocker
from app.common import lib as commonLib

# 이부분을 없애고 session에서 school_id 값 가져와서 바로 넘어가는걸로 바꿔야한다
@login_required
def schoolList():
    #if(request.method=='GET'): # get일때
    #    return render_template('schoolList.html')
    #elif(request.method=='POST'): # post 일때
    #school_id = request.form['school_id']

    article_school_id = commonLib.getCrawledSchoolId(g.db, session['school_id'])
    return redirect(url_for('lectureList', school_id=article_school_id))

@login_required
@cralwer_blocker
def lectureList():
    # 여기서 session으로 처리하기
    # school_id = commonLib.get_user_id(g.db, session['school_id'])
    school_id = request.values.get('school_id',None)
    print("***************")
    print(school_id)

    # if(request.method=='GET'):
    #     entries = TE.selectArticles(school_id)
    # elif(request.method=='POST'):
    search_word= request.values.get('search', None)
    if(search_word):
        search_item = "'%" + search_word + '%' + "'"
        entries = tempLE.selectArticlesWithSearchItem(school_id, search_item)
    elif(search_word == None or search_word == ''):
        entries = tempLE.selectArticles(school_id)

    translator = request.values.get('translator',None)
    if(translator):
        search_item = "'%" + translator + '%' + "'"
        entries = tempLE.selectArticlesWithTranslator(school_id, search_item)

    #여기서 검색결과가 하나도 없어서 entreis가 0으로 나왔을때 처리
    if(entries==()):
        entries = [{
             'who': '',
             'article_id': 0,
             'name': '',
             'professor': '',
             'text': '검색결과가 없습니다',
             'translated': '',
             'school_id': school_id
        }]
    return render_template('lectureList.html', entries=entries)

@login_required
@cralwer_blocker
def lectureDetail():
    lecture_id = request.values.get('lecture_id', None)
    entries = tempLE.selectLectureDetail(lecture_id)

    return render_template('lectureDetail.html',entries=entries)

@login_required
@cralwer_blocker
def updateTranslation():
    translated = request.form.get('translated', None)
    article_id = int(request.form.get('article_id', None))
    #who = request.form.get('who',None)
    if translated is None :
        return "뭐가 입력이 안됐다는 메시지를 출력해주면 될듯"
        # return make_response(jsonify(msg = "ㅇㅇㅇ가 입력되지 않았습니다",404

    is_ok = tempLE.updateTranslation(article_id,translated)

    if (is_ok == False):
        return "에러 메세지 출력해주기 !!!"

    lecture_id = int(request.form.get('lecture_id',None))
    # None 인 경우 각각 에러 처리 친절한 에러 메세지~ 출력하기
    return redirect(url_for('lectureDetail', lecture_id=lecture_id))

@login_required
@cralwer_blocker
def updateNewArticle():
    lecture_id = int(request.form.get('lecture_id',None))
    year = int(request.form.get('year',None))
    semester = request.form.get('semester',None)
    rate = float(request.form.get('rate',None))
    comment = request.form.get('comment',None)
    school_id = int(request.form.get('school_id',None))

    is_ok = tempLE.updateNewArticle(school_id, lecture_id, year, semester, rate, comment)

    if (is_ok == False):
        return "에러 메세지 출력해주기 !!!"

    return redirect(url_for('lectureDetail', lecture_id=lecture_id))
