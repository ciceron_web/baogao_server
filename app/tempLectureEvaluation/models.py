from flask import g, session
import traceback
import pymysql
from app.common import lib as commonLib


def selectArticles(school_id):
    curs = g.db.cursor()
    query = """
          select articles.who, articles.article_id, articles.lecture_id, lectures.name, lectures.professor, text, articles.translated, lectures.school_id 
          from lectures, articles 
          where lectures.lecture_id = articles.lecture_id and lectures.school_id=%s 
          group by lectures.lecture_id
          order by articles.created_at DESC LIMIT 500
          """
    curs.execute(query, (school_id))
    entries = curs.fetchall()

    return entries

def selectArticlesWithSearchItem(school_id,search_item):
    curs = g.db.cursor()
    query = """
          select articles.who, articles.article_id, lectures.name, articles.lecture_id, lectures.professor, text, articles.translated,lectures.school_id 
          from lectures,articles 
          where lectures.lecture_id = articles.lecture_id and lectures.school_id=%s and name like %s
          group by lectures.lecture_id
          order by articles.created_at DESC
          """ %(school_id, search_item)
    curs.execute(query)
    entries = curs.fetchall()  ## fetch 할 때 방식 통일시키기

    return entries

def selectArticlesWithTranslator(school_id, search_item):
    curs = g.db.cursor()
    query = """
          select articles.who, articles.article_id, lectures.name, articles.lecture_id, lectures.professor, text, articles.translated,lectures.school_id 
          from lectures,articles 
          where lectures.lecture_id = articles.lecture_id and lectures.school_id=%s and who like %s
          order by lectures.name, lectures.lecture_id
          """ %(school_id, search_item)
    curs.execute(query)
    entries = curs.fetchall()

    return entries

def selectLectureDetail(lecture_id):
    curs = g.db.cursor()
    query = """
          select articles.article_id, articles.lecture_id, lectures.school_id, articles.year, articles.semester, 
          articles.text, articles.translated, lectures.name, lectures.professor, articles.rate, articles.who
          from lectures,articles
          where lectures.lecture_id = articles.lecture_id and lectures.lecture_id=%s
          """ % (lecture_id)
    curs.execute(query)
    entries = curs.fetchall()

    return entries

def updateTranslation(article_id, translated):
    curs = g.db.cursor()
    query = """
            update articles 
            set translated=%s, user_id=%s, created_at=CURRENT_TIMESTAMP
            where article_id=%s
            """
    user_id = commonLib.get_user_id(g.db, session.get('email', None))

    try:
        curs.execute(query, (translated, user_id, article_id))
        if(curs.rowcount == -1) or (curs.rowcount == 0):
            g.db.rollback()
            return False # affected rows 받아오는 함수 있다!
    except:
        traceback.print_exc()
        g.db.rollback()
        return False
    g.db.commit()
    return True

def updateNewArticle(school_id, lecture_id, year, semester, rate, comment):
    curs = g.db.cursor()
    user_id = commonLib.get_user_id(g.db, session['email'])
    query = """
          INSERT INTO articles
          (school_id, lecture_id, year, semester, rate, user_id, translated, created_at)
          VALUES
          (%s,        %s,         %s,   %s,       %s,   %s,      %s,         current_timestamp)
          """
    try:
        curs.execute(query, (school_id, lecture_id, year, semester, rate, user_id, comment))
        if (curs.rowcount == -1) or (curs.rowcount == 0):
            g.db.rollback()
            return False  # affected rows 받아오는 함수 있다!
    except:
        traceback.print_exc()
        g.db.rollback()
        return False
    g.db.commit()
    return True
