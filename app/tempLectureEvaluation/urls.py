from app import app, versions
import app.tempLectureEvaluation.controllers as tempLE

app.add_url_rule('/evaluation2', view_func=tempLE.schoolList , methods=["GET",'POST'])
app.add_url_rule('/evaluation2/schools', view_func=tempLE.lectureList, methods = ['GET','POST'])
app.add_url_rule('/evaluation2/schools/articles', view_func=tempLE.updateTranslation, methods=['POST'])
app.add_url_rule('/evaluation2/schools/articleDetail', view_func=tempLE.lectureDetail, methods=['POST','GET'])
app.add_url_rule('/evaluation2/schools/newArticle', view_func=tempLE.updateNewArticle, methods=['POST'])